package main

import (
	"../shared"
	"net/rpc"
	"net"
	"log"
	"errors"
)

var globalVariants = map[int]string {
	1: "---",
	2: "---",
	3: "---",
	4: "---",
	5: "---",
	6: "---",
	7: "---",
	8: "---",
	9: "---",
	10: "---",
}

type VariantService struct{}

func (t *VariantService) GetVariants(args *shared.Args, reply *map[int]string) error {
	*reply = globalVariants
	return nil
}

func (t *VariantService) SelectVariant(args *shared.Args, reply *map[int]string) error {
	name := args.Name
	variant := args.Variant

	if variant < 0 || variant > 10 {
		return errors.New("wrong variant")
	}

	if val, ok := globalVariants[variant]; ok && val != "---" {
		return errors.New("variant already booked")
	}

	globalVariants[variant] = name

	*reply = globalVariants
	return nil
}

func registerVariantService(server *rpc.Server, service shared.VariantService) {
	server.RegisterName("VariantService", service)
}

func main() {
	service := new(VariantService)

	server := rpc.NewServer()
	registerVariantService(server, service)

	l, e := net.Listen("tcp", ":1234")
	if e != nil {
		log.Fatal("lister error:", e)
	}

	server.Accept(l)
}
