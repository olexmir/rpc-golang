package main

import (
	"net/rpc"
	"log"
	"net"
	"fmt"
	"../shared"
	"bufio"
	"os"
	"strings"
	"strconv"
	"sort"
)

type Client struct {
	client *rpc.Client
}

func (t *Client) GetVariants() map[int]string {
	args := &shared.Args{Name: "", Variant: -1}
	var reply map[int]string
	err := t.client.Call("VariantService.GetVariants", args, &reply)
	if err != nil {
		log.Fatal("variant error:", err)
	}
	return reply
}

func (t *Client) SelectVariant(variant int, name string) map[int]string {
	args := &shared.Args{Name: name, Variant: variant}
	var reply map[int]string
	err := t.client.Call("VariantService.SelectVariant", args, &reply)
	if err != nil {
		if err.Error() == "variant already booked" {
			fmt.Println("Variant already booked. Try again!")
		}
		fmt.Println(err.Error())
		return t.GetVariants()
	}
	return reply
}

func printMap(m map[int]string) {
	var keys []int
	for k := range m {
		keys = append(keys, k)
	}
	sort.Ints(keys)

	for _, k := range keys {
		fmt.Println("Key:", k, "Value:", m[k])
	}
}

func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Enter your group number")

	name, _ := reader.ReadString('\n')
	fmt.Println(name)

	conn, err := net.Dial("tcp", "localhost:1234")
	if err != nil {
		log.Fatal("Connection:", err)
	}

	client := &Client{client: rpc.NewClient(conn)}

	for {
		fmt.Println("Enter operation")
		op, _ := reader.ReadString('\n')
		op = strings.Replace(op, "\n", "", -1)

		if strings.Compare("1", op) == 0 {
			printMap(client.GetVariants())
		}

		if strings.Compare("2", op) == 0 {
			v, _ := reader.ReadString('\n')
			v = strings.Replace(v, "\n", "", -1)
			ve, _ := strconv.Atoi(v)
			printMap(client.SelectVariant(ve, name))
		}
	}

}

