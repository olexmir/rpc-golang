package shared

type Args struct {
	Name string
	Variant int
}

type VariantService interface {
	GetVariants(args *Args, reply *map[int]string) error
	SelectVariant(args *Args, reply *map[int]string) error
}
