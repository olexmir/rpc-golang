package main

import (
	"../shared"
	"errors"
	"fmt"
	"log"
	"net"
	"net/rpc"
)

type Arith int

func (t *Arith) Multiply(args *shared.Args, reply *int) error {
	fmt.Println("multiply", args.A, args.B)
	*reply = args.A * args.B
	return nil
}

func (t *Arith) Divide(args *shared.Args, quo *shared.Quotient) error {
	fmt.Println("dividing", args.A, args.B)
	if args.B == 0 {
		return errors.New("divide by zero")
	}
	quo.Quo = args.A / args.B
	quo.Rem = args.A % args.B
	return nil
}

func registerArith(server *rpc.Server, arith shared.Arith) {
	server.RegisterName("Arithmetic", arith)
}

func main() {
	arith := new(Arith)

	server := rpc.NewServer()
	registerArith(server, arith)

	l, e := net.Listen("tcp", ":1234")
	if e != nil {
		log.Fatal("listen error:", e)
	}

	fmt.Println("Server is running...")

	server.Accept(l)
}
